<img align="right" width="200" height="200" src="https://codeberg.org/repo-avatars/17075-2d1ec69a58985a346840bd32b28feb85">

# Diagramme

Hier lagern Diagramme, die dem „Diagrams as Code“ bzw. dem „Documentation as Code“-Ansatz folgen. Dieser besagt, dass Dokumentaionen mit Werkzeugen aus der Software-Entwicklung erstellt und verwaltet werden sollten. Beispiele für entsprechende Programme sind u. a. *[PlantUML](https://plantuml.com/de/)*, *[Diagrams](https://diagrams.mingrammer.com/)* oder *[AsciiDoc](https://asciidoc.org/)*.

Diese Diagramme können z. B. ins [Vereinswiki](https://wiki.computertruhe.de/) eingebunden werden.